package cn.test.sec;

import org.springframework.security.access.annotation.Secured;

public interface TestI {
	// @RolesAllowed({ "ROLE_ADMIN" })

	// @PreAuthorize("hasRole('ROLE_ADMIN')")
	@Secured("ROLE_ADMIN")
	public void print();
}
