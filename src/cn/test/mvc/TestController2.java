package cn.test.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("label")
public class TestController2 {

	@RequestMapping(method = RequestMethod.GET)
	public String test() {
		return "getTest2";

	}

}
