package cn.test.mvc;

import javax.annotation.security.RolesAllowed;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.test.sec.TestI;
import cn.test.sec.TestImpl;

@Controller
@RequestMapping("page")
public class TestController {

	@RequestMapping(method = RequestMethod.GET)
	@Secured("ROLE_TELLER")
	public String testGet() {

		TestI t = new TestImpl();
		t.print();

		return "getTest";

	}

	@RequestMapping(method = RequestMethod.POST)
	public String testPost() {
		System.out.println("POST");
		return "getTest";

	}

}
