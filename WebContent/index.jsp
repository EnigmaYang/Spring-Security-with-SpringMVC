<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<security:authorize access="hasRole('ROLE_ADMIN')">
	<a href="/SpringMVCTest/page">goTo Page Test</a><br>
	</security:authorize>
	<a href="/SpringMVCTest/label">goTo Label Test</a><br>
	<a href="/SpringMVCTest/logout">logout</a><br>
</body>
</html>